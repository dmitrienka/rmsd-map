package ineos
import java.io._
import com.github.tototoshi.csv._
import breeze.linalg._
import breeze.numerics._
import ineos.Utils._

object Utils {

  def readData (filename: String) : Map[String,List[Array[Double]]] = {
    val raw_csv = CSVReader.open(new File(filename)).allWithHeaders()
    raw_csv.groupBy( _("ID"))
      .mapValues( l => l.map( x => Array(x("X").toDouble, x("Y").toDouble, x("Z").toDouble   )))
  }

  def readIsms (filename: String) : List[List[Int]] = {
    CSVReader.open(new File(filename)).all()
      .map(_.map(_.toInt))
  }

  def prepareData(data: Map[String,List[Array[Double]]], isms: List[List[Int]] ) =
    data.mapValues( r => isms.map(ism => DenseMatrix(ism.map(i => r(i - 1)):_*)    ))

  def kabschImproperRMSD(a: DenseMatrix[Double], b: DenseMatrix[Double]) : Double = {
    val n = a.rows
    val svd.SVD(_,d,_) =  svd(a.t * b)
    val rmsd = (sum(pow(a,2)) + sum(pow(b,2)) - 2*sum(d)) / n 
    if (rmsd < 0) 0 else sqrt(rmsd)
  }

  def bestRMSD(a : DenseMatrix[Double], lb: List[DenseMatrix[Double]]) =
    lb.map(kabschImproperRMSD(a, _)).min


  def combinations2[A](l: List[A]): Iterator[(A, A)] =
    l.tails.flatMap(_ match {
      case h :: t => t.iterator.map((h, _))
      case Nil => Iterator.empty
    })


  def calcDists(data: Map[String,List[breeze.linalg.DenseMatrix[Double]]],
    chunkSize: Int, outfile: String) = {

    def f(c: (String, String)) =
      List(c._1 , c._2 , bestRMSD(data( c._1 )(0), data( c._2  )))


    val keys = combinations2(data.keys.toList).grouped(chunkSize)


    val writer = CSVWriter.open(outfile, append=true)


    keys.foreach { l =>
      {
        val cur = System.nanoTime()
        writer.writeAll(l.par.map(f).toList)
        val times = (System.nanoTime() - cur)/ 1000000000
        println(times)
      }
    }
    writer.close()
  }
}

object Main extends App {
  val data = prepareData(readData(args(0)), readIsms(args(1)))
  println("Data prepared!")
  calcDists(data, 10000000, "out.rms")
}
